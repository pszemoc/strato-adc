#include "adc.hpp"
#include <wiringPiI2C.h>
#include <cstring>
#include <stdexcept>
#include <thread>
#include <chrono>

adafruit::adc_t::adc_t() {
  device_handler = wiringPiI2CSetup(adafruit::adc::address);
  if (device_handler < 0) {
    throw std::runtime_error(std::strerror(errno));
  }
}

adafruit::adc_t::~adc_t() {

}

void adafruit::adc_t::config (int config_reg) {
  wiringPiI2CWriteReg16(device_handler, adafruit::adc::reg::config, config_reg);
  int fs_config = config_reg & adafruit::adc::reg::config_pga_mask;
  switch (fs_config)
  {
  case adafruit::adc::reg::config_pga_6_144V:
    fs = 6.144;
    break;
  case adafruit::adc::reg::config_pga_4_096V:
    fs = 4.096;
    break;
  case adafruit::adc::reg::config_pga_2_048V:
    fs = 2.048;
    break;
  case adafruit::adc::reg::config_pga_1_024V:
    fs = 1.024;
    break;
  case adafruit::adc::reg::config_pga_0_512V:
    fs = 0.512;
    break;
  case adafruit::adc::reg::config_pga_0_256V:
    fs = 0.256;
    break;
  default:
    fs = 4.096;
    break;
  }
}

double adafruit::adc_t::read_conversion() {
  int16_t res = wiringPiI2CReadReg16(device_handler, adafruit::adc::reg::convert); 
  res = res / 16;
  return static_cast<double>(res) / 1000.0 / fs;
}

double adafruit::adc_t::read_a0() {
  int16_t res = wiringPiI2CReadReg16(device_handler, adafruit::adc::reg::config);
  res = (~adafruit::adc::reg::config_mux_mask & res) | adafruit::adc::reg::config_mux_single_0 | adafruit::adc::reg::config_os_single;
  wiringPiI2CWriteReg16(device_handler, adafruit::adc::reg::config, res);
  std::this_thread::sleep_for(std::chrono::milliseconds(20));
  return read_conversion();
}

double adafruit::adc_t::read_a1() {
  int16_t res = wiringPiI2CReadReg16(device_handler, adafruit::adc::reg::config);
  res = (~adafruit::adc::reg::config_mux_mask & res) | adafruit::adc::reg::config_mux_single_1 | adafruit::adc::reg::config_os_single;
  wiringPiI2CWriteReg16(device_handler, adafruit::adc::reg::config, res);
  std::this_thread::sleep_for(std::chrono::milliseconds(20));
}

double adafruit::adc_t::read_a2() {
  int16_t res = wiringPiI2CReadReg16(device_handler, adafruit::adc::reg::config);
  res = (~adafruit::adc::reg::config_mux_mask & res) | adafruit::adc::reg::config_mux_single_2 | adafruit::adc::reg::config_os_single;
  wiringPiI2CWriteReg16(device_handler, adafruit::adc::reg::config, res);
  std::this_thread::sleep_for(std::chrono::milliseconds(20));
  return read_conversion();
}

double adafruit::adc_t::read_a3() {
  int16_t res = wiringPiI2CReadReg16(device_handler, adafruit::adc::reg::config);
  res = (~adafruit::adc::reg::config_mux_mask & res) | adafruit::adc::reg::config_mux_single_3 | adafruit::adc::reg::config_os_single;
  wiringPiI2CWriteReg16(device_handler, adafruit::adc::reg::config, res);
  std::this_thread::sleep_for(std::chrono::milliseconds(20));
  return read_conversion();
}