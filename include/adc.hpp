#pragma once
#include <stdint.h>

namespace adafruit {
namespace adc {
  constexpr int address = 0x48;
  namespace reg {
    constexpr int convert = 0x00;
    constexpr int config  = 0x01;

    constexpr int config_os_mask      = 0x8000;
    constexpr int config_os_single    = 0x8000;
    constexpr int config_os_busy      = 0x0000;
    constexpr int config_os_notbusy   = 0x8000;

    constexpr int config_mux_mask     = 0x7000;
    constexpr int config_mux_diff_0_1 = 0x0000;
    constexpr int config_mux_diff_0_3 = 0x1000;
    constexpr int config_mux_diff_1_3 = 0x2000;
    constexpr int config_mux_diff_2_3 = 0x3000;
    constexpr int config_mux_single_0 = 0x4000;
    constexpr int config_mux_single_1 = 0x5000;
    constexpr int config_mux_single_2 = 0x6000;
    constexpr int config_mux_single_3 = 0x7000;

    constexpr int config_pga_mask     = 0x0e00;
    constexpr int config_pga_6_144V   = 0x0000;
    constexpr int config_pga_4_096V   = 0x0200;
    constexpr int config_pga_2_048V   = 0x0400;
    constexpr int config_pga_1_024V   = 0x0600;
    constexpr int config_pga_0_512V   = 0x0800;
    constexpr int config_pga_0_256V   = 0x0a00;

    constexpr int config_mode_mask    = 0x0100;
    constexpr int config_mode_contin  = 0x0000;
    constexpr int config_mode_single  = 0x0100;

    constexpr int config_dr_max       = 0x00e0;
    constexpr int config_dr_128sps    = 0x0000;
    constexpr int config_dr_250sps    = 0x0020;
    constexpr int config_dr_490sps    = 0x0040;
    constexpr int config_dr_920sps    = 0x0060;
    constexpr int config_dr_1600sps   = 0x0080;
    constexpr int config_dr_2400sps   = 0x00a0;
    constexpr int config_dr_3300sps   = 0x00c0;

    constexpr int config_cque_mask    = 0x0003;
    constexpr int config_cque_1conv   = 0x0000;
    constexpr int config_cque_2conv   = 0x0001;
    constexpr int config_cque_4conv   = 0x0002;
    constexpr int config_cque_none    = 0x0003;
  }
}
  
class adc_t {
public:

private:
  int32_t device_handler;
  double fs;

public:
  adc_t();
  ~adc_t();

  void config (int config_reg);
  double read_conversion();
  double read_a0();
  double read_a1();
  double read_a2();
  double read_a3();
};
}