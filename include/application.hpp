#include "adc.hpp"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h"
#include "strato-frames/pressure_frame.hpp"
#include <memory>
#include <zmq.hpp>
#include <libconfig.h++>
#include <csignal>
#include <stdexcept>

class Application {
private:
  zmq::context_t                    context;
  std::shared_ptr<zmq::socket_t>    socket;
  std::shared_ptr<adafruit::adc_t>  adc;

  inline static bool running = false;
   static void termination_handler(int) {
    spdlog::info("Termination requested");
    running = false;
  }

  void init_log() {
    auto file_logger = spdlog::basic_logger_mt("adc", "/var/log/balloon/adc.log");
    spdlog::set_default_logger(file_logger);
    spdlog::default_logger()->flush_on(spdlog::level::level_enum::info);
  }

  void init_signals() {
    std::signal(SIGTERM, Application::termination_handler);
  }

  void init_socket() {
    libconfig::Config cfg;
    try {
      cfg.readFile("/etc/sp-config/sp.cfg");
    } catch (const libconfig::FileIOException &fioex) {
      throw std::runtime_error("I/O error while reading file.");
    } catch (const libconfig::ParseException &pex) {
      std::stringstream ss;
      ss << "Parse error at " << pex.getFile() << ":" << pex.getLine()
            << " - " << pex.getError() << std::endl;
      throw std::runtime_error(ss.str().c_str());
    }

    const libconfig::Setting& root = cfg.getRoot();
    int port;
    if (!root["adc"].lookupValue("adc_port", port)) {
      throw std::runtime_error("ADC port not defined in config.");
    }
    spdlog::info("Config parsed.");

    socket = std::make_shared<zmq::socket_t>(context, ZMQ_PUB);
    if (socket != nullptr) {
      socket->bind("tcp://*:" + std::to_string(port));
    } else {
      throw std::runtime_error("Error while creating socket.");
    }
    spdlog::info("Socket opened at: {}", port);
  }

public:
  Application() : context(1) {}    

  int exec() {
    running = true;
    init_log();
    init_signals();
    try {
      init_socket();
      adc = std::make_shared<adafruit::adc_t>();
    } catch (const std::exception& e) {
      spdlog::error(e.what());
      running = false;
      return -1;
    }
    adc->config(adafruit::adc::reg::config_pga_4_096V    |
                adafruit::adc::reg::config_dr_128sps     |
                adafruit::adc::reg::config_mode_single   |
                adafruit::adc::reg::config_mux_single_0  |
                adafruit::adc::reg::config_cque_none     
    );

    while (running) {
      std::this_thread::sleep_for(std::chrono::seconds(1)); 

      double val = adc->read_a0();
      pressure_frame_t pressure_frame;
      pressure_frame.pressure = 103421.359 * (val / 3.3); // max range [Pa] * ( adc_voltage / ref_voltage ) 
      spdlog::info("Pressure: {} Pa", pressure_frame.pressure);
      try {
        socket->send(frame_topic::pressure.begin(), frame_topic::pressure.end(), ZMQ_SNDMORE);
        socket->send(zmq::const_buffer(&pressure_frame, sizeof(pressure_frame_t)));
      } catch (const std::exception& e) {
        spdlog::error(e.what());
      }
    }
    return 0;
  }

};
