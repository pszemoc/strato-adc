CXX		  := g++
CXX_FLAGS := -Wall -Wextra -std=c++17 -ggdb

BIN		:= bin
SRC		:= src
INCLUDE	:= include
LIB		:= lib

LIBRARIES	:= -lpthread -lwiringPi -lzmq -lconfig++
EXECUTABLE	:= main


all: $(BIN)/$(EXECUTABLE)

run: clean all
	clear
	./$(BIN)/$(EXECUTABLE)

$(BIN)/$(EXECUTABLE): $(SRC)/*.cpp
	mkdir -p bin
	$(CXX) $(CXX_FLAGS) -I$(INCLUDE) -L$(LIB) $^ -o $@ $(LIBRARIES)

clean:
	-rm $(BIN)/*

install:
	sudo mkdir -p /var/log/balloon
	sudo cp ./$(BIN)/$(EXECUTABLE) /usr/bin/sp-adc
	sudo cp sp-adc.service /etc/systemd/system

uninstall:
	sudo rm /usr/bin/sp-adc /etc/systemd/system/sp-adc.service

